class profile::puppetdb {

  #class { 'puppetdb::database::postgresql': }
  #class { 'puppetdb::server': }
  class { 'puppetdb':
    manage_firewall => false,
  }

}
