class profile::base {
    include commonbase
    include accounts
    include apt
    include ntp
    include ssh
}
