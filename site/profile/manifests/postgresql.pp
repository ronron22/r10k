class profile::postgresql {
  
  $listen_addresses = hiera('profile::postgresql::listen_addresses')

  class { 'postgresql::server':

    listen_addresses => $listen_addresses,

  }

}

