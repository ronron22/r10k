class role::mutu_slave inherits role {

    include profile::mail
    include profile::web
    include profile::php_fpm
    include profile::dns
    include profile::haproxy
    include profile::mumble
    include profile::postgresql
    include profile::firewall
    include test_architux
    include test_architux::ouap

}
