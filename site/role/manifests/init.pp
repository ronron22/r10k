class role {

  ## begin test ## 
  $istagada = hiera_array('istagada', undef)
  # la variable role étant un fact, il ne faut pas aller la chercher dans hiera..
  #$isdns = hiera('isdns')

  $grr = $facts['foo_content']
  notify {"rr: $grr": }
  #notify { "roles: $roles": }
  notify { "role1: $istagada": }
  #notify { "role2: $factsisdns": }
  ## end test ##

  notify {"role is: -==| $role |==-": }

  include profile::base

}
